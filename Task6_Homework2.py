from datetime import date


class MyClass1:


    def __init__(self, surname, name, age):
        self.surname = surname
        self.name = name
        self.age = age

    @classmethod
    def fromBirthYear(cls, surname, name, birthYear):
        return cls(surname, name, date.today().year - birthYear)

    def print_info(self):
        print(self.surname + " " + self.name + "'s age is: " + str(self.age))


class MyClass2(MyClass1):
    _instance = None

    def __new__(cls, age):
        if cls._instance is None:
            cls.count_ukraine = 0
            cls.count_usa_19 = 0
            cls.count_usa_18 = 0
        return object.__new__(cls)


    def __init__(self, age):
        super().__init__(age)


    @classmethod
    def adult_order(cls, age):
        if age >= 18:
            cls.count_usa_18 += 1
        elif cls.age >= 19:
            cls.count_usa_19 += 1
        elif cls.age >= 21:
            cls.count_ukraine += 1
        return cls(age, count_ukraine, count_usa_19, count_usa_18)

    @classmethod
    def order_final(cls):
        print(f"Next people are adult in Ukraine {cls.count_ukraine}")


m_per1 = MyClass1('Ivanenko', 'Ivan', 19)
m_per1.print_info()

m_per2 = MyClass1.fromBirthYear('Dovzhenko', 'Bogdan',  2000)
m_per2.print_info()

m_per3 = MyClass1.fromBirthYear('Sydorchuk', 'Petro', 2010)
print(isinstance(m_per3, MyClass2))

m_per4 = MyClass1.fromBirthYear('Makuschenko', 'Dmytro', 2001)
print(isinstance(m_per4, MyClass1))

print(issubclass(MyClass1, MyClass2))
print(issubclass(MyClass2, MyClass1))

print(MyClass2.__dict__)
MyClass2.order_final()